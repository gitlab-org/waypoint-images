# HashiCorp Waypoint Images

This project provides docker images containing HashiCorp's Waypoint binary, providing the oppurtunity to do any bootstrapping necessary for it to work in GitLab CI templates and excludes an entrypoint unlike the [official image from Hashicorp](https://hub.docker.com/r/hashicorp/waypoint/tags), which currently breaks CI jobs. A scheduled pipeline runs at 9PM UTC to update all images and create for new versions.

## Waypoint Versions Supported
Images for all versions are generated with the most recent version also getting tagged as `latest`. They are all also built with `docker:latest` as the base image. You can see all the image tags in this project's [container registry page](https://gitlab.com/gitlab-org/waypoint-images/container_registry).

## How to contribute? 
Contributions are always welcome. Don't be shy!
If there's no other issue already discussing what you want, simply open a new issue and the maintainers will gladly review it and come back to you as soon as possible. If there's an open issue with the "Accepting merge requests" label, simply open up a merge request proposal linking to that issue and we'll also review it as soon as possible.
